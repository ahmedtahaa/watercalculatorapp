/**
 * 
 */
package com.fxpro.water.calculate.validator;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;


/**
 * @author AhmedTaha
 *
 */
public class WaterCalculatorValidatorTest{
	private WaterCalculatorValidator  waterCalculatorValidator;
	
	@Before
	public void init() {
		waterCalculatorValidator= new WaterCalculatorValidatorImpl();
	}

	 @Test
	    public void landscapeisNotEmpty() {
	        try {
	        	int [] landscape={5,2,3,4,5,4,0,3,1};
	        	waterCalculatorValidator.validate(landscape);
	        } catch (FxproWaterCalculatorException e) {
	        	   fail("Should have been valid and therefore not thrown an exception");
	        }
	    }
	 @Test
	    public void landscapeSizeExceedMaxSize() {
		 int [] landscape= new int [35003];
	        try {
	        	for(int i=0;i<35003;i++) {
	        		landscape[i]=i;	
	        	}
	        	waterCalculatorValidator.validate(landscape);
	        	fail("Should have had FxproWaterCalculatorException containing 'Landscape Size'");
	        } catch (FxproWaterCalculatorException e) {
	        	 assertTrue(e.getMessage().contains("Landscape Size"));
	        }
	    }
	 @Test
	    public void landscapeisHaveInvalidPositions() {
	        try {
	        	int [] landscape={5,2,3,4,5,45000,0,3,-1};
	        	waterCalculatorValidator.validate(landscape);
	            fail("Should have had FxproWaterCalculatorException containing 'invalid Position'");
	        } catch (FxproWaterCalculatorException e) {
	            assertTrue(e.getMessage().contains("Landscape Contains invalid Position"));
	        }
	    }
	 @Test
	    public void nullLandscape() {
	        try {
	        	int [] landscape=null;
	        	waterCalculatorValidator.validate(landscape);
	            fail("Should have had FxproWaterCalculatorException containing 'should not be null'");
	        } catch (FxproWaterCalculatorException e) {
	            assertTrue(e.getMessage().contains("should not be null"));
	        }
	    }
	 @Test
	    public void landscapeisHaveInvalidHeight() {
	        try {
	        	int [] landscape={5,2,3,4,5,4,0,48000,1};
	        	waterCalculatorValidator.validate(landscape);
	            fail("Should have had FxproWaterCalculatorException containing 'invalid Position'");
	        } catch (FxproWaterCalculatorException e) {
	        	assertTrue(e.getMessage().contains("Landscape Contains invalid Position"));
	        }
	    }

}
