/**
 * 
 */
package com.fxpro.water.calculate.proxy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;
import com.fxpro.water.calculate.service.WaterCalculatorMainService;
/**
 * @author AhmedTaha
 *
 */
class WaterCalcultorProxyTest {

	@Test
	void test() {
		WaterCalculatorMainService mainService= WaterCalculatorServiceProxyFactory.newInstance();
		assertTrue(mainService!=null);
	}
	@Test
	public void testValidResult() throws FxproWaterCalculatorException {
		int[] input = { 5, 2, 3, 4, 5, 4, 0, 3, 1 };
		long result = WaterCalculatorServiceProxyFactory.newInstance().calculateWaterAmount(input);
		assertEquals(9, result);
		
	}

}
