package com.fxpro.water.calculate;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;
import com.fxpro.water.calculate.proxy.WaterCalculatorServiceProxyFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Rigourous Test :-)
	 * 
	 * @throws FxproWaterCalculatorException
	 */
	public void testApp() throws FxproWaterCalculatorException {
		int[] input = { 5, 2, 3, 4, 5, 4, 0, 3, 1 };
		long result = WaterCalculatorServiceProxyFactory.newInstance().calculateWaterAmount(input);
		assertEquals(9, result);
	}
}
