package com.fxpro.water.calculate.service;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;




public class WaterCalculatorServiceTest {
	private WaterCalculatorService waterCalculatorService = null;

	@Before
	public void init() {

		waterCalculatorService = new WaterCalculatorService();
	}

	@Test
	public void testCalculateWaterAmount() throws FxproWaterCalculatorException {
		int landscape [] = {5,2,3,4,5,4,0,3,1};
		waterCalculatorService.calculateWaterAmount(landscape);
		assertEquals(9,waterCalculatorService.calculateWaterAmount(landscape));
	}
	@Test
	public void testCalculateWaterAmountValidResult() throws FxproWaterCalculatorException {
		int landscape [] = {5,1,3,6,5,2,0,3,1};
		waterCalculatorService.calculateWaterAmount(landscape);
		assertEquals(10,waterCalculatorService.calculateWaterAmount(landscape));
	}
	@Test
	public void testCalculateWaterAmountValidationTest() {
		try {
	    	int [] landscape={5,2,3,4,5,45000,0,3,-1};
	    	waterCalculatorService.calculateWaterAmount(landscape);
	        fail("Should have had FxproWaterCalculatorException containing 'invalid Position'");
	    } catch (FxproWaterCalculatorException e) {
	        assertTrue(e.getMessage().contains("Landscape Contains invalid Position"));
	    }

	}
	@Test
    public void nullLandscape() {
        try {
        	int [] landscape=null;
        	waterCalculatorService.calculateWaterAmount(landscape);
            fail("Should have had FxproWaterCalculatorException containing 'should not be null'");
        } catch (FxproWaterCalculatorException e) {
            assertTrue(e.getMessage().contains("should not be null"));
        }
    }
	
	
}
