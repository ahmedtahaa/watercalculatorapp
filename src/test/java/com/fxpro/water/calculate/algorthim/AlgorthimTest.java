/**
 * 
 */
package com.fxpro.water.calculate.algorthim;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.fxpro.water.calculate.core.algorithm.WaterCalculatorAlgorithm;

/**
 * @author AhmedTaha
 *
 */
public class AlgorthimTest {
	private WaterCalculatorAlgorithm waterCalculatorAlgorithm = null;

	@Before
	public void init() {
		waterCalculatorAlgorithm = new WaterCalculatorAlgorithm();

	}

	@Test
	public void test() {
		assertEquals(0, waterCalculatorAlgorithm.getWaterVolumesNumber((new int[] { 9, 8, 7 })));
		assertEquals(9, waterCalculatorAlgorithm.getWaterVolumesNumber((new int[] { 5,2,3,4,5,4,0,3,1 })));
		assertEquals(9, waterCalculatorAlgorithm.getWaterVolumesNumber((new int[] { 5,1,3,10,5,4,0,3,1 })));
		assertEquals(0, waterCalculatorAlgorithm.getWaterVolumesNumber((new int[] { 9, 8, -7 })));
	}
}
