package com.fxpro.water.calculate.util;

/**
 * This class for store static values which i can reference it from one location
 * 
 * @author AhmedTaha
 *
 */
public class Defines {

	public static final String LOGS = "logs";
	public static final String WATER_CALCULATOR_LOGS = "WaterCalculator-logs";
	public static final String WATER_CALCULATOR_APP = "watercalculator";
	public static final String WATER_CALCULATOR_VALIDATION_MSG_LANDSCAPE_MAXSIZE = " Landscape Size Shouldnot exceed 32000 Position ";
	public static final String WATER_CALCULATOR_VALIDATION_MSG_LANDSCAPE_EMPTY = " Landscape Shouldn't be Empty ";
	public static final String WATER_CALCULATOR_VALIDATION_MSG_FIRST_PART = " Landscape Contains invalid Position Height  which it's value is ";
	public static final String WATER_CALCULATOR_VALIDATION_MSG_SECOND_PART = " and valid Height Must be more than or equal 0 or and less than or equal: 32000  ";
	public static final String REQUIRED_MSG = "Landscape Array is Required it should not be null";

	private Defines() {
		throw new IllegalStateException("Defines class");
	}
}
