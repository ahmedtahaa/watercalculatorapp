package com.fxpro.water.calculate.log;

import com.fxpro.water.calculate.util.Defines;

/**
 * This factory class responsible for create the Logger object and i apply
 * singleton Design pattern on it to return only one created instance which
 * write in one file or the same file
 * 
 * @author AhmedTaha
 *
 */
public class WaterCalculatorLoggerFactory {

	private static WaterCalculatorLogger logger;

	private WaterCalculatorLoggerFactory() {
		throw new IllegalStateException("WaterCalculatorLoggerFactory class");
	}

	static {
		logger = new WaterCalculatorLogger(WaterCalculatorLogger.class, Defines.LOGS, Defines.WATER_CALCULATOR_LOGS,
				Defines.WATER_CALCULATOR_APP);
	}

	/**
	 * this for return singleton logger
	 * @return WaterCalculatorLogger
	 */
	public synchronized static WaterCalculatorLogger getLogger() {
		return logger;
	}
}
