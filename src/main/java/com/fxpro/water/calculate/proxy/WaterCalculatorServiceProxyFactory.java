package com.fxpro.water.calculate.proxy;

import com.fxpro.water.calculate.service.WaterCalculatorMainService;
import com.fxpro.water.calculate.service.WaterCalculatorMainServiceImpl;

/**
 * this class for create proxy instance
 * 
 * @author AhmedTaha
 *
 */
public class WaterCalculatorServiceProxyFactory {

	private WaterCalculatorServiceProxyFactory() {
		throw new IllegalStateException("WaterCalculatorServiceProxyFactory class");
	}

	public static WaterCalculatorMainService newInstance() {
		return WaterCalculatorServiceProxy.createServiceInstance(new WaterCalculatorMainServiceImpl());
	}
}