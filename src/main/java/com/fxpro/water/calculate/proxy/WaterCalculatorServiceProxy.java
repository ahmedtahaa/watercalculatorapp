package com.fxpro.water.calculate.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;


import org.apache.log4j.Level;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;
import com.fxpro.water.calculate.exception.FxproWaterCalculatorExceptionUtil;
import com.fxpro.water.calculate.log.WaterCalculatorLogger;
import com.fxpro.water.calculate.log.WaterCalculatorLoggerFactory;
import com.fxpro.water.calculate.service.WaterCalculatorMainService;




/**
 * This Class is Dynamic Proxy  and InvocationHandler implementation 
 * use java reflection to log parameters and method name 
 * this the proxy of APP  
 * @author AhmedTaha
 *
 */
public class WaterCalculatorServiceProxy implements InvocationHandler {

	private WaterCalculatorMainService waterCalculatorMainService;
	private WaterCalculatorLogger logger = WaterCalculatorLoggerFactory.getLogger();

	private WaterCalculatorServiceProxy(WaterCalculatorMainService waterCalculatorMainService) {
		this.waterCalculatorMainService = waterCalculatorMainService;
	}

	public static WaterCalculatorMainService createServiceInstance(WaterCalculatorMainService waterCalculatorMainService) {

		return (WaterCalculatorMainService) Proxy.newProxyInstance(waterCalculatorMainService.getClass().getClassLoader(),
				waterCalculatorMainService.getClass().getInterfaces(), new WaterCalculatorServiceProxy(waterCalculatorMainService));
	}
	
	/**
	 * This Method responsible proceed the calls of any method and it is like interceptor 
	 * we can add logs here in one place handle exception in one place 
	 * convert method parameters to json to be trackable and readable
	 * @param Object : This is the Proxy instance on which method is invoked
	 * @param Method: This corresponds to interface method which is invoked on proxy instance.
	 * @param Object[]: It contains an array of arguments passed in method invocation.
	 * @return the result of Execution
	 * @throws FxproWaterCalculatorException
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		try {
			logger.log(Level.INFO, "::Execute Method==> " + method.getName());
			StringBuilder stringCarrier = new StringBuilder("");
			for (int i = 0; args != null && i < args.length; i++) {
				if (args[i]!=null&&args[i] instanceof int []) {
					Arrays.stream((int [])args[i]).forEach(position->{
						stringCarrier.append(position + ", ");
					});
				}	
			}
			if(stringCarrier.toString()!=null && !stringCarrier.toString().equals("")) {
				logger.log(Level.INFO, "::Parameters of " + method.getName() + " is : " +stringCarrier.toString().substring(0, stringCarrier.toString().length() - 2));
			}else{
				logger.log(Level.INFO, "::Parameters of " + method.getName() + " is : "+stringCarrier.toString());	
			}
			result = method.invoke(waterCalculatorMainService, args);
		} catch (Exception ex) {
			logger.log(Level.ERROR, "Error During Executing "+method.getName(), ex);
			throw new FxproWaterCalculatorException((ex instanceof  FxproWaterCalculatorException?((FxproWaterCalculatorException)ex).getCode()
					:FxproWaterCalculatorExceptionUtil.GENERAL_EXCEPTION), ex.getMessage(), ex);
		}
		return result;
	}
}
