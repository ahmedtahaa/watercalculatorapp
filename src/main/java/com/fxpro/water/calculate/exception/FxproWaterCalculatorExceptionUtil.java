package com.fxpro.water.calculate.exception;



/**
 /* This Class for wrap our FxproWaterCalculatorException Class and Custom error code and custom messages
 * <p>
 * you can in your front end get code of each Exception and check on it.
 * </p>
 * @author AhmedTaha
 *
 */
public class FxproWaterCalculatorExceptionUtil {

	public final static String GENERAL_EXCEPTION = "WATERCALCULATOR-101";
	public final static String INCORRECT_PARAMETER_EXCEPTION = "WATERCALCULATOR-104";
	public final static String GENERAL_EXCEPTION_MSG = "General Error Please Try Valid Inputs";


	public FxproWaterCalculatorExceptionUtil() {
		super();
	}

	/**
	 * This Method to wrap Throwable to FxproWaterCalculatorException
	 * Add Custom Error Code
	 * @param thr
	 * @return FxproWaterCalculatorException
	 */
	public static final FxproWaterCalculatorException wrap(Throwable thr) {
		return new FxproWaterCalculatorException(FxproWaterCalculatorExceptionUtil.GENERAL_EXCEPTION, thr.getMessage(), thr);
	}
}
