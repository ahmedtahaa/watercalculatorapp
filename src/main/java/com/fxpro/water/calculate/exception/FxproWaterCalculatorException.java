package com.fxpro.water.calculate.exception;

import org.apache.commons.lang.exception.ExceptionUtils;



/**
 * This our Wrapper Exception Object Which Extends {@link} Exception
 * Any exception occurs will be thrown As FxproWaterCalculatorException such like validation errors and so on
 * @author AhmedTaha
 */
public class FxproWaterCalculatorException extends Exception {

	private static final long serialVersionUID = 1L;

	protected String code;
	protected String message;
	protected String rootCause;

	public FxproWaterCalculatorException(String code, String message, Throwable thr) {
		super(thr);
		this.message = message;
		this.code = code;
		this.rootCause = ExceptionUtils.getStackTrace(thr);
	}

	public FxproWaterCalculatorException(String code, String message) {

		this.message = message;
		this.code = code;
	}

	public FxproWaterCalculatorException(FxproWaterCalculatorException e) {
		super(e);
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public String getRootCause() {
		return rootCause;
	}
}
