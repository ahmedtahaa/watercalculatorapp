package com.fxpro.water.calculate.validator;

/**
 * This class will be used to statically call these validation methods. In the
 * code below, the validations are declared using the functional interface  
 * @author AhmedTaha
 *
 */

public class ValidatorUtil {
	public static final double MAX_LANDSCAPE_POSITIONS = 32000;
	public static final int MAX_LANDSCAPE_POSITIONS_HEIGHT = 32000;
	public static final int MIN_LANDSCAPE_POSITIONS_HEIGHT = 0;

	public static final GenericValidator<Integer> notNegativeNumber = GenericValidatorImpl.from(val -> val < 0);
	public static final GenericValidator<Integer[]> notEmptyArray = GenericValidatorImpl.from(val -> val.length != 0);
	public static final GenericValidator<Integer[]> notNull = GenericValidatorImpl.from(val -> val != null);
	public static final GenericValidator<Integer[]> notExceedMaxPositions = GenericValidatorImpl.from(val -> val.length < 32000);
	public static final GenericValidator<Integer> lessThanMaxHeight = GenericValidatorImpl.from(val -> val <= MAX_LANDSCAPE_POSITIONS_HEIGHT);
	public static final GenericValidator<Integer> moreThanMinHeight = GenericValidatorImpl.from(val -> val <= MIN_LANDSCAPE_POSITIONS_HEIGHT);
	
	  private ValidatorUtil() {
		    throw new IllegalStateException("ValidatorUtil class");
		  }
    public static final GenericValidator <Integer> integerMoreThan(int limit) {
        return GenericValidatorImpl.from(s -> s >= limit);
    }
    public static final GenericValidator <Integer> integerLessThan(int size) {
        return GenericValidatorImpl.from(s -> s <= size);
    }
	public static final GenericValidator <Integer> positionBetween(int morethan, int lessThan) {
        return integerMoreThan(morethan).and(integerLessThan(lessThan));
    }

}
