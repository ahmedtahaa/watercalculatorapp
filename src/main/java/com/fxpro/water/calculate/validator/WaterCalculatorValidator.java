/**
 * 
 */
package com.fxpro.water.calculate.validator;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;

/**
 * This interface for landscape Validation to validate all constrains 
 * @author AhmedTaha
 *
 */
public interface WaterCalculatorValidator {
	public void validate(int [] landscape) throws FxproWaterCalculatorException;
}
