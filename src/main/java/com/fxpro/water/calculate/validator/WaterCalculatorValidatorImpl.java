/**
 * 
 */
package com.fxpro.water.calculate.validator;

import java.util.Arrays;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;
import com.fxpro.water.calculate.exception.FxproWaterCalculatorExceptionUtil;
import com.fxpro.water.calculate.util.Defines;

/**
 * This is class is the implementation of WaterCalculatorValidator we will validate all
 * landscape constrains
 * 
 * @author AhmedTaha
 *
 */
public class WaterCalculatorValidatorImpl implements WaterCalculatorValidator {

	/**
	 * This Method to validate landscape Array according to our constrains the
	 * errors will be added on string builder and check if not empty i will throw
	 * FxproWaterCalculatorException Exception Every error have full Description
	 * message
	 * 
	 * @param landscape
	 * @throw FxproWaterCalculatorException
	 */
	@Override
	public void validate(int[] landscape) throws FxproWaterCalculatorException {
		StringBuilder errorFields = new StringBuilder();
		if(landscape!=null) {
		Integer[] boxedLandscape = Arrays.stream(landscape).boxed().toArray(Integer[]::new);
		errorFields.append(ValidatorUtil.notEmptyArray.validate(boxedLandscape)
				.getFieldNameIfInvalid(Defines.WATER_CALCULATOR_VALIDATION_MSG_LANDSCAPE_EMPTY + "\n").orElse(""));
		errorFields.append(ValidatorUtil.notExceedMaxPositions.validate(boxedLandscape)
				.getFieldNameIfInvalid(Defines.WATER_CALCULATOR_VALIDATION_MSG_LANDSCAPE_MAXSIZE + "\n").orElse(""));

		Arrays.stream(landscape).forEach(position -> {
			errorFields.append((ValidatorUtil.positionBetween(ValidatorUtil.MIN_LANDSCAPE_POSITIONS_HEIGHT,
					ValidatorUtil.MAX_LANDSCAPE_POSITIONS_HEIGHT)).validate(position)
							.getFieldNameIfInvalid(Defines.WATER_CALCULATOR_VALIDATION_MSG_FIRST_PART + position
									+ Defines.WATER_CALCULATOR_VALIDATION_MSG_SECOND_PART + "\n")
							.orElse(""));
		});
		}else {
			errorFields.append(Defines.REQUIRED_MSG);
		}
		String errors = errorFields.toString();
		if (!errors.isEmpty()) {
			throw new FxproWaterCalculatorException(FxproWaterCalculatorExceptionUtil.INCORRECT_PARAMETER_EXCEPTION,
					errors);
		}
	}
}
