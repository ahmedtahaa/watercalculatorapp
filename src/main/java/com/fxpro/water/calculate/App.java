package com.fxpro.water.calculate;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;
import com.fxpro.water.calculate.proxy.WaterCalculatorServiceProxyFactory;

/**
 * For testing!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	int [] input = { 5, 2, 3, 4, 5, 4, 0, 3, 1 };
        try {
			System.out.println(WaterCalculatorServiceProxyFactory.newInstance().calculateWaterAmount(input) );
		} catch (FxproWaterCalculatorException e) {
			e.printStackTrace();
		}
    }
}
