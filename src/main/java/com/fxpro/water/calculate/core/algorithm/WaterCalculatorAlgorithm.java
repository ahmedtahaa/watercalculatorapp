package com.fxpro.water.calculate.core.algorithm;


/**
 * This class where i write my algorithm 
 * @author AhmedTaha
 */
public class WaterCalculatorAlgorithm {
	/*
	 * @desc inside this Method i apply the Algorithm which got the numbers of
	 * volumes we can got volume of water in case of rain  if there are higher position on left and
	 * right. the amount of water to stored by every element by finding the heights
	 * of position on left and right sides. and then loop over all elements and got the
	 * smallest side left and right position and subtract it from element value and
	 * return to my sum
	 * 
	 * @parameter it takes landscape as Input
	 * 
	 * @return number of volumes
	 */
	public long getWaterVolumesNumber(int[] landscape) {
		long waterVolume = 0;
		int [] leftLandscapePositions = new int[landscape.length];
		int [] rightLandscapePositions = new int[landscape.length];

		leftLandscapePositions[0] = landscape[0];
		for (int i = 1; i < landscape.length; i++)
			leftLandscapePositions[i] = Math.max(leftLandscapePositions[i - 1], landscape[i]);

		rightLandscapePositions[landscape.length - 1] = landscape[landscape.length - 1];
		for (int i = landscape.length - 2; i >= 0; i--)
			rightLandscapePositions[i] = Math.max(rightLandscapePositions[i + 1], landscape[i]);

		for (int i = 0; i < landscape.length; i++)
			waterVolume += Math.min(leftLandscapePositions[i], rightLandscapePositions[i]) - landscape[i];

		return waterVolume;
	}

}
