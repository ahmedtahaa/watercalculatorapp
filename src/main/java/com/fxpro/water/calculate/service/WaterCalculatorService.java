/**
 * 
 */
package com.fxpro.water.calculate.service;

import com.fxpro.water.calculate.core.algorithm.WaterCalculatorAlgorithm;
import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;
import com.fxpro.water.calculate.validator.WaterCalculatorValidatorImpl;

/**
 * Due to applying Facade design pattern this service class 
 * of WaterCalculator Algorithm which will bypass the request to {@link} WaterCalculatorAlgorithm
 * to return the number of water volumes 
 * @author AhmedTaha
 *
 */
public class WaterCalculatorService {

	public long calculateWaterAmount(int[] landscape) throws FxproWaterCalculatorException {
		new WaterCalculatorValidatorImpl().validate(landscape);
		return new WaterCalculatorAlgorithm().getWaterVolumesNumber(landscape);
	}
}
