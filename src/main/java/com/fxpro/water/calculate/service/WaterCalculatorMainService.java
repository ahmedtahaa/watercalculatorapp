/**
 * 
 */
package com.fxpro.water.calculate.service;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;

/**
 * This the WaterCalculatorMainService interface which contain the public access method 
 * accessed by proxy 
 * this interface will be the Facade Pattern Main Service
 * @author AhmedTaha
 *
 */
public interface WaterCalculatorMainService {
	public long calculateWaterAmount(int[] landscape)throws FxproWaterCalculatorException;
}
