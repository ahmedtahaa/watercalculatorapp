/**
 * 
 */
package com.fxpro.water.calculate.service;

import com.fxpro.water.calculate.exception.FxproWaterCalculatorException;

/**
 * This Class is the implementation of 
 * {@link} WaterCalculatorMainService
 * @author AhmedTaha
 *
 */
public class WaterCalculatorMainServiceImpl implements WaterCalculatorMainService {

	@Override
	public long calculateWaterAmount(int[] landscape) throws FxproWaterCalculatorException {
		return new WaterCalculatorService().calculateWaterAmount(landscape);
	}

}
